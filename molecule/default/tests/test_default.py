import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("cs_studio")


def test_conda_env_csstudio_exists(host):
    assert host.file("/opt/conda/envs/csstudio/bin").exists
